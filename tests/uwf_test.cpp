//
// Created by alfonzso on 2016. 12. 31..
//
#include "gtest/gtest.h"
#include <gmock/gmock.h>
#include "../sources/uwf.cpp"

using testing::Eq;

namespace {
    class class_dec : public testing::Test {
    public:
        uwf obj;

        class_dec() {
            obj; // shit
        }
    };
}

TEST_F(class_dec, get_use_win_frame_value_from_conf) {
    std::vector<std::string> conf_vector_str;
    conf_vector_str.push_back("UseWindow=adada");
    conf_vector_str.push_back("UseWindowFrame=False");
    conf_vector_str.push_back("adadadad=adada");
    uwf u;
    u.setConf_vector_str(conf_vector_str);
    ASSERT_EQ(u.get_use_win_frame_from_conf(), "UseWindowFrame=False");
}

int main(int argc, char *argv[]) {

    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    return 0;
}