#include <string.h>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <string>
#include <conio.h>
#include <sstream>
#include <sys/stat.h>
#define cls system("cls");

//const char L2TXT[] = "xL2TXT";
const char decoded_l2_ini[] = "#decoded_l2_ini.data";
const char l2_backup_file_name[] = "l2_ini.bak";
const char config[] = "#config";
const char l2encdec[] = "tools\\l2encdec.exe";
const char libgmp[] = "tools\\libgmp-3.dll";

enum {
    KEY_ESC = 27,
    F1 = 256 + 59,
    F2 = 256 + 60
};

bool file_exists(const std::string &name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

bool file_not_exists(const std::string &name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) != 0);
}
