#include <iostream>
#include "uwf.h"

using namespace std;

//#include "gtest/gtest.h"
//#include "gmock/gmock.h"

int main(int argc, char *argv[]) {
/*
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    return 0;
*/

    uwf o_uwf;
    o_uwf.init_files();
    o_uwf.read_config_file();
    o_uwf.l2ini_encode();
    o_uwf.menu();

    return 0;
}
