//
// Created by alfonzso on 2016. 12. 31..
//

#include "main.h"
#include <iostream>
#include <shlwapi.h>
#include "uwf.h"

using namespace std;


uwf::uwf() {
    char dest[MAX_PATH];
    GetModuleFileName(NULL, dest, (sizeof(dest)));
    PathRemoveFileSpec(dest);
    this->real_path = dest;
}

void uwf::read_config_file() {
    std::string temp_line = "";
    std::ifstream in(config);
    while (!in.eof()) {
        getline(in, temp_line);
        conf_vector_str.push_back(temp_line);
    }
    in.close();

}

void uwf::init_files() {
    if (!file_exists(l2_backup_file_name)) {
        std::cerr << "\nNincs meg az l2_ini.bak fajl, press ENTER";
        std::cin.get();
        exit(-1);
    }
    if (file_not_exists(config)) {
        std::cerr << "\nNincs meg a config fajl, press ENTER";
        std::cin.get();
        exit(-1);
    }
    if (file_not_exists(l2encdec)) {
        std::cerr << "\nNincs meg az l2encdec.exe, press ENTER";
        std::cin.get();
        exit(-1);
    }
    if (file_not_exists(libgmp)) {
        std::cerr << "\nNincs meg a libgmp.dll, press ENTER";
        std::cin.get();
        exit(-1);
    }
}

void uwf::l2ini_encode() {

    std::string l2_encode =
            std::string(l2encdec) + " " + std::string(l2_backup_file_name) + " " + std::string(decoded_l2_ini);

    std::cout << "File removed : " << (remove(decoded_l2_ini) == 0) ? "Ok" : "Nope";

    system((l2_encode + " -l ").c_str());

    cls
    if (file_not_exists(decoded_l2_ini)) {
        system((l2_encode + " -s ").c_str());
        cls
        std::cout << "Az ini nem eredeti fajlnak tunik (nem patchel-t), megprobalom masik kapcsoloval.... ";
        if (file_not_exists(decoded_l2_ini)) {
            std::cerr << "Nem sikerult a converzio";
            std::cin.get();
            exit(-1);
        }
    }
}

void uwf::l2_decode() {
    int i = 0;
    char n0 = '\"';
    char i0[5];
    std::string l2encode = std::string(l2encdec) + " -h 413 " + std::string(decoded_l2_ini) + " L2.ini";
    std::string l2e = std::string(l2encdec) + " -h ", nc0 = " xL2TXT L2.ini";

    std::cout << "\n\tESC = Kilepes atalakitas nelkul\n\tF1 = -h 413\n\tF2 = -h valasztas";

    int c = getch();

    if (c == 0 || c == 224)
        c = 256 + getch();

    switch (c) {
        case KEY_ESC     : {
            exit(0);
        }
        case F1          : {
            system(l2encode.c_str());
            break;
        }
        case F2          : {
            std::cout << "\tHeader fajl: ";
            std::cin >> i;
            itoa(i, i0, 10);
            l2e += i0 + nc0;
            l2e = n0 + l2e + n0;
            system(l2e.c_str());
            break;
        }
        default:
            l2_decode();
            break;

    }
}

std::string uwf::get_use_win_frame_from_conf() {
    for (std::size_t i = 0; i < conf_vector_str.size(); ++i) {
        if (conf_vector_str.at(i).find("UseWindowFrame")
            != std::string::npos) {
            return conf_vector_str.at(i);
        };
    }
    return "";
}

std::string uwf::get_cache_size_from_conf() {
    for (std::size_t i = 0; i < conf_vector_str.size(); ++i) {
        if (conf_vector_str.at(i).find("CacheSizeMegs")
            != std::string::npos) {
            return conf_vector_str.at(i);
        };
    }
    return "";
}

std::string uwf::get_distance_from_conf() {
    for (std::size_t i = 0; i < conf_vector_str.size(); ++i) {
        if (conf_vector_str.at(i).find("Dist")
            != std::string::npos) {
            return conf_vector_str.at(i);
        };
    }
    return "";
}

void uwf::UseWindowsFrame() {
    l2txt_vector_str[x0 - 1] = get_use_win_frame_from_conf();

    std::ofstream out(decoded_l2_ini);
    std::size_t size = l2txt_vector_str.size();
    for (std::size_t i = 0; i < size; ++i) {
        out << l2txt_vector_str[i] << std::endl;
    }
    out.close();
    cls
    menu();
}

void uwf::CacheSizeMegs() {

    l2txt_vector_str[x1 - 1] = get_cache_size_from_conf();

    std::ofstream out(decoded_l2_ini);
    std::size_t size = l2txt_vector_str.size();
    for (std::size_t i = 0; i < size; ++i) {
        out << l2txt_vector_str[i] << std::endl;
    }
    out.close();
    cls
    menu();
}

void uwf::Dist() {
    l2txt_vector_str[x2 - 1] = get_distance_from_conf();

    std::ofstream out(decoded_l2_ini);
    std::size_t size = l2txt_vector_str.size();
    for (std::size_t i = 0; i < size; ++i) {
        out << l2txt_vector_str[i] << std::endl;
    }
    out.close();
    cls
    menu();
}

void uwf::menu() {
    l2txt = "";
    l2txt_vector_str.clear();
    int i = 0;
    char c;
    x0 = 0;
    x1 = 0;
    x2 = 0;
    a = '*';
    std::ifstream in(decoded_l2_ini);
    std::size_t found0, found1, found2;
    while (!in.eof()) {

        getline(in, l2txt);
        i++;

        found0 = l2txt.find("UseWindowFrame");
        found1 = l2txt.find("CacheSizeMegs");
        found2 = l2txt.find("Dist");
        if (found0 != std::string::npos) {
            if (x0 == 0) {
                x0 = i;
            }
        }
        if (found1 != std::string::npos) {
            if (x1 == 0) {
                x1 = i;
            }
        }
        if (found2 != std::string::npos) {
            if (x2 == 0) {
                x2 = i;
            }
        }
        l2txt_vector_str.push_back(l2txt);
    }
    in.close();

//UWF=?=CUWF//
    if (l2txt_vector_str[x0 - 1] == conf_vector_str[2]) {
        std::cout << "\n\n( 1 ) U.W.F.: True  -> False  [" << a << "]\n";
    } else {
        std::cout << "\n\n( 1 ) U.W.F.: True  -> False  [x]\n";
    }
//CacheSizeMegs=?=CCacheSizeMegs//
    if (l2txt_vector_str[x1 - 1] == conf_vector_str[3]) {
        std::cout << " ( 2 ) Cache change:32 -> 512 [" << a << "] \n";
    } else {
        std::cout << " ( 2 ) Cache change:32 -> 512 [x] \n";
    }
//Dist=?=CDist//
    if (l2txt_vector_str[x2 - 1] == conf_vector_str[4]) {
        std::cout << "  ( 3 ) Dist: 1000  ->  9000  [" << a << "] \n";
    } else {
        std::cout << "  ( 3 ) Dist: 1000  ->  9000  [x] \n";
    }
    std::cout << "   ( ESC ) L2TXT to L2.ini / EXIT";
    c = getch();

    switch (c) {
        case '1'     : {
            UseWindowsFrame();
            break;
        }
        case '2'     : {
            CacheSizeMegs();
            break;
        }
        case '3'     : {
            Dist();
            break;
        }
        case 27      : {
            l2_decode();
            break;
        }
        default:
            cls
            menu();
            break;
    }
}

const vector<string> &uwf::getConf_vector_str() const {
    return conf_vector_str;
}

void uwf::setConf_vector_str(const vector<string> &conf_vector_str) {
    uwf::conf_vector_str = conf_vector_str;
}
