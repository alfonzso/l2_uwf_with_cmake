//
// Created by alfonzso on 2016. 12. 31..
//

using namespace std;

#include "vector"
#ifndef UWF_UWF_H
#define UWF_UWF_H

class uwf {
    int x0, x1, x2;
    char a;
    std::string l2txt;
    std::vector<std::string> l2txt_vector_str;
    std::vector<std::string> conf_vector_str;
public:
    const vector<string> &getConf_vector_str() const;

    void setConf_vector_str(const vector<string> &conf_vector_str);

    uwf();

    void init_files();

    void l2ini_encode();

    void l2_decode();

    void menu();

    void read_config_file();

    void UseWindowsFrame();

    void CacheSizeMegs();

    void Dist();

    std::string real_path;

    std::string get_use_win_frame_from_conf();

    string get_cache_size_from_conf();

    string get_distance_from_conf();
};

#endif //UWF_UWF_H
